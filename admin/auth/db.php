<?php 

$login = function() use ($conn) {

    $mail = filter_input(INPUT_POST, 'mail');
    $password = filter_input(INPUT_POST, 'password');

    if(is_null($mail) or is_null($password)){
        var_dump('parou');
        return false;
    }

    $sql = 'SELECT * FROM users WHERE mail = ?';
    $stmt = $conn->prepare($sql);
    $stmt->bind_param('s', $mail);

    $stmt->execute();

    $result = $stmt->get_result();
    $user = $result->fetch_assoc();

    if(!$user){
        return false;
    }

    if(password_verify($password, $user['password'])){
        unset($user['password']);
        $_SESSION['auth'] = $user;
        return true;
    }

    return false;

};