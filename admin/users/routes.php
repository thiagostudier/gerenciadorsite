<?php 

include __DIR__ . '/db.php';

if( resolve('/admin/users') ){

    $users = $users_all();

    render('admin/users/index', 'admin', compact('users'));

}elseif( resolve('/admin/users/create') ){

    if($_SERVER['REQUEST_METHOD'] === 'POST'){
        $user_create();
        return header('location: /admin/users');
    }

    render('admin/users/create', 'admin');

}elseif( $params = resolve('/admin/users/(\d+)') ){

    $data = $user_view($params[1]);

    render('admin/users/view', 'admin', $data);

}elseif( $params = resolve('/admin/users/(\d+)/edit') ){
    
    $data = $user_view($params[1]);

    if($_SERVER['REQUEST_METHOD'] === 'POST'){
        $user_edit($params[1]);
        return header('location: /admin/users');
    }

    render('admin/users/edit', 'admin', $data);

}elseif( $params = resolve('/admin/users/(\d+)/delete') ){

    $user_delete($params[1]);

    return header('location: /admin/users');

}else{
    http_response_code(404);
    echo 'Página não encontrada';
}