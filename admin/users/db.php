<?php 

function users_get_data ($redirectOnError){
    $mail = filter_input(INPUT_POST, 'mail');

    if(is_null($mail)){
        flash('PREENCHA OS CAMPOaS CORRETAMENTE','error');
        header('location: '.$redirectOnError);
        die();
    }
    
    return compact('mail');
}

$users_all = function() use ($conn){
    $result = $conn->query('SELECT * FROM users');
    return $result->fetch_all(MYSQLI_ASSOC);
};

$user_view = function($id) use ($conn){

    $sql = 'SELECT * FROM users WHERE id = ?';

    $stmt = $conn->prepare($sql);
    $stmt->bind_param('i', $id);

    $stmt->execute();

    $result = $stmt->get_result();
    return $result->fetch_assoc();

};

$user_create = function() use ($conn){
    $data = users_get_data('/admin/users/create');

    // SE O USUÁRIO NÃO INSERIU UMA SENHA
    if(is_null(filter_input(INPUT_POST, 'password'))){
        flash('INSIRA UMA SENHA','error');
        header('location: '.$redirectOnError);
        die();
    }else{
        $data['password'] = filter_input(INPUT_POST, 'password');
    }

    $sql = 'INSERT INTO users(mail, password, created, updated) VALUES (?,?,NOW(),NOW())';

    $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);

    $stmt = $conn->prepare($sql); 
    $stmt->bind_param('ss', $data['mail'], $data['password']);

    flash('Usuário criado com sucesso!');

    return $stmt->execute();

};

$user_edit = function($id) use ($conn){

    $data = users_get_data('/admin/users/' .$id. '/edit');
    $data['password'] = filter_input(INPUT_POST, 'password');
    // VERIFICAR SE O USUÁRIO INSERIU A SENHA
    if(is_null($data['password']) || $data['password'] == ''){
        $sql = 'UPDATE users set mail = ?, updated= NOW() WHERE id = ?';
    }else{
        $sql = 'UPDATE users set mail = ?, password = ?, updated= NOW() WHERE id = ?';
    }

    $stmt = $conn->prepare($sql);
    // VERIFICAR SE O USUÁRIO INSERIU A SENHA
    if(is_null($data['password']) || $data['password'] == ''){
        $stmt->bind_param('si', $data['mail'], $id);
    }else{
        $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
        $stmt->bind_param('ssi', $data['mail'], $data['password'], $id);
    }

    return $stmt->execute();

};

$user_delete = function($id) use ($conn){

    $sql = 'DELETE FROM users WHERE id = ?';

    $stmt = $conn->prepare($sql);
    $stmt->bind_param('i', $id);

    flash('Usuário removido com sucesso!');

    return $stmt->execute();

};

