<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>Site</title>
    <!-- Pnotify -->
    <link rel="stylesheet" href="/pnotify/pnotify.css">
    <!-- CSS -->
    <link rel="stylesheet" href="/site.css" />
</head>
<body>

    <header id="header">
        <h1>Bem vindo a SON</h1>
    </header>

    <ul id="nav">
        <?php foreach($data['pages'] as $page){ ?>
            <li><a href="/<?=$page['url']?>"><?=$page['title']?></a></li>
        <?php } ?>
    </ul>

    <main id="content">
        <br />
        <?php include $content; ?>
    </main>

    <p id="footer"><small><?=date('Y')?> - todos os direitos reservados</small></p>
        
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="/pnotify/pnotify.js"></script>
    
    <script>
        
        <?php flash(); ?>
    
    </script>


</body>
</html>