<h3 class="mb-5 ">Visualizar usuário</h3>

<dl class="rol">
    <dt class="col-sm-3">Email</dt>
    <dd class="col-sm-9"><?=$data['mail']?></dd>

    <dt class="col-sm-3">Criado em</dt>
    <dd class="col-sm-9"><?=$data['created']?></dd>

    <dt class="col-sm-3">Atualizado em</dt>
    <dd class="col-sm-9"><?=$data['updated']?></dd>
</dl>

<p>
    <a href="/admin/users/<?=$data['id']?>/edit" class="btn btn-primary">Editar</a>
    <a href="/admin/users/<?=$data['id']?>/delete" class="btn btn-danger confirm">Remover</a>
</p>

<a href="/admi"></a>