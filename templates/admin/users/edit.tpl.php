<h3 class="mb-5">Atualizar usuário</h3>

<form method="POST" autocomplete="off">

    <div class="form-group">
    
        <label for="email">E-mail</label>
        <input value="<?=$data['mail']?>" type="mail" id="email" name="mail" class="form-control" placeholder="Informe o e-mail" />

    </div>

    <div class="form-group">

        <label for="password">Senha: <?=$data['password']?></label>
        <input value="" type="password" id="password" name="password" class="form-control">

    </div>

    <button type="submit" class="btn btn-primary">Salvar</button>

</form>

<hr>

<a href="/admin/users/1" class="btn btn-secondary">Voltar</a>