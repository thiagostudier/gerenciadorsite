<h3 class="mb-5">Criar usuário</h3>

<form method="POST">

    <div class="form-group">
        <label for="email">E-mail</label>
        <input type="mail" id="email" name="mail" class="form-control" placeholder="Informe o e-mail" />
    </div>

    <div class="form-group">
        <label for="password">Senha</label>
        <input type="password" id="password" name="password" class="form-control">
    </div>

    <button type="submit" class="btn btn-primary">Salvar</button>

</form>

<hr>

<a href="/admin/users" class="btn btn-secondary">Voltar</a>