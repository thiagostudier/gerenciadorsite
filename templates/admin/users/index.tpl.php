<h3 class="mb-5">Administração de usuários</h3>
<!-- LISTAGEM DAS PÁGINAS -->

<table class="table table-hover">
    <thead>
        <tr>
            <th>#</th>
            <th>E-mail</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($data['users'] as $user){ ?>
        <tr>
            <td><?=$user['id']?></td>
            <td><?=$user['mail']?></td>
            <td class="text-right">
                <a href="/admin/users/<?=$user['id']?>" class="btn btn-primary btn-sm">Ver</a>
                <a href="/admin/users/<?=$user['id']?>/edit" class="btn btn-primary btn-sm">Editar</a>
                <a href="/admin/users/<?=$user['id']?>/delete" class="btn btn-danger btn-sm confirm">Deletar</a>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>

<a href="/admin/users/create" class="btn btn-secondary">Novo</a>