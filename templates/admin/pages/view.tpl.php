<h3 class="mb-5 ">Visualizar página</h3>

<div class="row">
    <div class="col-3 mb-2">
        <div class="row">
            <dt class="col-sm-5 small">Título</dt>
            <dd class="col-sm small"><?=$data['page']['title']?></dd>

            <dt class="col-sm-5 small">URL</dt>
            <dd class="col-sm small"><?=$data['page']['url']?> - <a href="/<?=$data['page']['url']?>" target="_blank">abrir</a></dd>

            <dt class="col-sm-5 small">Criado em</dt>
            <dd class="col-sm small"><?=$data['page']['created']?></dd>

            <dt class="col-sm-5 small">Atualizado em</dt>
            <dd class="col-sm small"><?=$data['page']['updated']?></dd>
        </div>
    </div>
    <div class="col bg-light p-3">
        <?=$data['page']['body']?>
    </div>
</div>

<p>
    <a href="/admin/pages/<?=$data['page']['id']?>/edit" class="btn btn-primary">Editar</a>
    <a href="/admin/pages/<?=$data['page']['id']?>/delete" class="btn btn-danger confirm">Remover</a>
</p>

<a href="/admin/pages" class="btn btn-secondary">Voltar</a>