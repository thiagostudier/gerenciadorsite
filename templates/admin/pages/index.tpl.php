<h3 class="mb-5">Administração de páginas</h3>
<!-- LISTAGEM DAS PÁGINAS -->

<table class="table table-hover">
    <thead>
        <tr>
            <th>#</th>
            <th>Título</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($data['pages'] as $page){ ?>
        <tr>
            <th><?php echo $page['id']; ?></th>
            <th>
                <a href="/admin/pages/<?php echo $page['id']; ?>"><?php echo $page['title']; ?></a>
            </th>
            <th class="text-right">
                <a href="/admin/pages/<?php echo $page['id']; ?>" class="btn btn-primary btn-sm">Ver</a>
                <a href="/admin/pages/<?php echo $page['id']; ?>/edit" class="btn btn-primary btn-sm">Editar</a>
                <a href="/admin/pages/<?php echo $page['id']; ?>/delete" class="btn btn-danger btn-sm confirm">Deletar</a>
            </th>
        </tr>
    <?php } ?>
    </tbody>
</table>

<a href="/admin/pages/create" class="btn btn-secondary">Novo</a>