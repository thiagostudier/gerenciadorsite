<h3 class="mb-5">Criar página</h3>

<form action="" method="POST" class="">

    <div class="form-group">
        <label for="title">Título</label>
        <input type="text" name="title" id="title" placeholder="Título da sua página..." class="form-control" required>
    </div>

    <div class="form-group">
        <label for="url">URL</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text">/</span>
            </div>
            <input type="text" name="url" id="url" class="form-control" placeholder="URL...">
        </div>
    </div>

    <div class="form-group">
        <input type="hidden" id="body" name="body">
        <trix-editor input="body"></trix-editor>
    </div>

    <button type="submit" class="btn btn-primary">Salvar</button>

</form>

<hr>

<a href="/admin/pages" class="btn btn-secondary">Voltar</a>