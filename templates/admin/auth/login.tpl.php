<div class="card ">
    <div class="card-body">
        
        <h5>Login</h5>

        <form method="post">
            <div class="form-group">
                <label for="email">E-mail</label>
                <input type="mail" id="email" name="mail" class="form-control" placeholder="Informe o e-mail" />
            </div>

            <div class="form-group">
                <label for="password">Senha</label>
                <input type="password" id="password" name="password" class="form-control">
            </div>

            <button type="submit" class="btn btn-primary">Acessar</button>
        </form>

    </div>
</div>