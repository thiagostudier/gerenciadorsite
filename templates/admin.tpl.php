<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Pnotify -->
    <link rel="stylesheet" href="/pnotify/pnotify.css">
    <!-- Trix CSS -->
    <link rel="stylesheet" href="/trix/trix.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- FontAwsome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <!-- Style CSS -->
    <link rel="stylesheet" href="/css/style.css">
    <title>Admin</title>
  </head>
  <body class="d-flex flex-column">
    <div id="header">
        <nav class="navbar navbar-dark bg-dark">
            
            <span>
                <a href="/admin" class="navbar-brand">AdminSON</a>
                <span class="navbar-text">
                    Painel Administrativo da School of Net
                </span>
            </span>
            <a href="/admin/auth/logout" class="btn btn-danger"><i class="fa fa-sign-out" aria-hidden="true"></i></a>
            
        </nav>
    </div>
    <div id="main">
        <div class="row">
            <div class="col">
                <ul id="main-menu" class="nav flex-column nav-pills bg-secondary text-white p-2">
                    <li class="nav-item"><span href="#" class="nav-link text-white-50 small">MENU</span></li>
                    <li class="nav-item"><a href="/admin/pages" class="nav-link <?=( resolve('/admin/pages.*') ? 'active' : '') ?>"><i class="fa fa-files-o" aria-hidden="true"></i> Páginas</a></li>
                    <li class="nav-item"><a href="/admin/users" class="nav-link <?=( resolve('/admin/users.*') ? 'active' : '') ?>"><i class="fa fa-users" aria-hidden="true"></i> Usuários</a></li>
                </ul>    
            </div>
            <div class="col-10">
                <div id="content">
                    <?php include $content; ?>
                </div> 
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <script src="/pnotify/pnotify.js"></script>
    <script src="/trix/trix.js"></script>

    <script>

        document.addEventListener('trix-attachment-add', function(){
            
            const attachment = event.attachment;
            
            if(!attachment.file) {
                return;
            }
            
            const form = new FormData();
            form.append('file', attachment.file);

            $.ajax({
                url: '/admin/upload/image',
                method: 'POST',
                data: form,
                contentType: false,
                processData: false,
                xhr: function() {
                    const xhr = $.ajaxSettings.xhr();
                    xhr.upload.addEventListener('progress', function(e){
                        let progress = e.loaded / e.total * 100;
                        attachment.setUploadProgress(progress);
                    });
                    return xhr;
                }
            }).done(function(response){
                console.log(response);
                attachment.setAttributes({
                    url: response,
                    href: response,
                });
            }).fail(function(){
                console.log('deu erro!');
            });
        })
        
        <?php flash(); ?>

        const confirmEl = document.querySelector('.confirm');
        if(confirmEl){
            confirmEl.addEventListener('click', function(e) {
                e.preventDefault();
                if(confirm('Tem certeza que quer remover?')){
                    window.location = e.target.getAttribute('href');
                }
            });
        }

    </script>

  </body>
</html>