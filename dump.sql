create table users(
	id int not null AUTO_INCREMENT primary key,
    mail varchar(250) not null unique,
    password varchar(250) not null,
    created datetime not null,
    updated datetime not null
);

create table pages(
	id int not null AUTO_INCREMENT primary key,
    title varchar(250) not null,
    url varchar(250) not null,
    body text,
    created datetime not null,
    updated datetime not null
)