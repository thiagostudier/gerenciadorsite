<?php

function setInternalServerError($errno = null, $errstr = null, $errfile = null, $errline = null){

    http_response_code(500);

    echo "<h1>Error</h1>";

    // SE O DEBUG ESTIVER HABILITADO
    if(!DEBUG){
        exit;
    }

    switch($errno){
        case E_USER_ERROR:
            echo '<b>Erro:</b> ['.$errno.'] '.$errstr.'<br/>';
            echo 'Fatal error on line '. $errline .' in file '. $errfile;
            break;
        case E_USER_WARNING:
            echo '<b>Warning:</b> ['.$errno.'] '.$errstr.'<br/>';
            break;
        case E_USER_NOTICE:
            echo '<b>Notice:</b> ['.$errno.'] '.$errstr.'<br/>';
            break;
        default:
            echo 'Unknow error type: ['.$errno.'] '.$errstr.'<br/>';
            break;
    }

    echo '<ul>';
    foreach(debug_backtrace() as $error){
        if(!empty($error['file'])){
            echo '<li>';
            echo $error['file'] . ':';
            echo $error['line'];
            echo '</li>';
        }
    }
    echo '</ul>';
    exit;
}

set_error_handler('setInternalServerError');
set_exception_handler('setInternalServerError');